import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
} from 'react-native';

import Routes from './src/Routes';
import { colors } from './src/constant/style';

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{flex: 1,backgroundColor: colors.bgColor}}>
        <Routes />
      </SafeAreaView>
    </>
  )
}

export default App;
