import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import RandomizedPerson from './screens/RandomizedPerson';
import TaskRequest from './screens/TaskRequest';


const Routes = createStackNavigator({
  TaskRequest: TaskRequest,
  RandomizedPerson: RandomizedPerson,
}, {
  headerMode: 'none'
});

export default createAppContainer(Routes)