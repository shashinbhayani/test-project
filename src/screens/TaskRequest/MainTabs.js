import React, { useState } from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { setWidth, setHeight } from '../../utils';
import { colors } from '../../constant/style';


export default MainTabs = () => {
  const [activeTab, setActiveTab] = useState(3)
  return (
    <View style={styles.wrapper}>
      <Tab image={require('./../../assets/images/navbar/Path.png')} value={1} isActive={activeTab == 1} onPress={setActiveTab} />
      <Tab image={require('./../../assets/images/navbar/Group6.png')} value={2} isActive={activeTab == 2} onPress={setActiveTab}/>
      <Tab image={require('./../../assets/images/navbar/Shape.png')} value={3} isActive={activeTab == 3} onPress={setActiveTab}/>
      <Tab image={require('./../../assets/images/navbar/Group.png')} value={4} isActive={activeTab == 4} onPress={setActiveTab}/>
    </View>
  )
}

const Tab = ({image, isActive, onPress, value}) => {
  return (
    <TouchableOpacity 
      style={[styles.tabWrapper, {borderBottomColor: isActive ? 'transparent' : colors.borderColor}]}
      onPress={() => onPress(value)}
    >
      <Image source={image} style={{width: setWidth('5'), height: setWidth('5')}} />
      <View 
        style={[styles.tabBorder,
          {
            borderRightColor: isActive ? colors.borderColor : 'transparent',
            borderLeftColor: isActive ? colors.borderColor : 'transparent',
          }
        ]}
      />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    width: setWidth('100'),
    backgroundColor: '#FFFFFF'
  },
  tabWrapper: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
    paddingVertical: setHeight('3')
  },
  tabBorder: {
    height: 5,
    width: "100%",
    borderRightWidth: 1,
    borderLeftWidth: 1,
    position: "absolute",
    bottom: -1
  }
})