import React from 'react';
import { View, Text, Platform, StyleSheet, Image, TouchableHighlight } from 'react-native';
import { setHeight, setWidth } from '../../utils';
import { fonts } from '../../constant/style';
 
const headerHeight = Platform.OS == 'ios' ? 64 : 56

export default Header = ({navigation}) => {
  return (
    <View style={styles.headerWrapper}>
      <Image source={require('../../assets/images/Activity.png')} />
      <Text style={styles.title}>{'Task Request'}</Text>
      <TouchableHighlight onPress={() => navigation.navigate('RandomizedPerson')}>
      <Image source={require('../../assets/images/header-user/Rectangle.png')} style={{height: setWidth('7'), width: setWidth('7')}} resizeMode="contain"  />
      </TouchableHighlight>
    </View>
  )
}

const styles = StyleSheet.create({
  headerWrapper: {
    height: headerHeight,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: setWidth(3),
    alignItems: 'center'
  },
  title: {
    fontFamily: fonts.primaryMedium,
    fontSize: 21,
    color: '#1C252A'
  }
})