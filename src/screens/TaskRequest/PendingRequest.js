import React from 'react';
import { View, StyleSheet, Image, Text, FlatList } from 'react-native';
import { colors, fonts } from '../../constant/style';
import { setWidth, setHeight } from '../../utils';

const pendingRequestList = [
  {
    firstUser: {
      id: 'temp1',
      image: require('./../../assets/images/user12.png'),
      name: 'Amanda Frost',
      occupation: 'School Teacher',
      experience: '7yrs exp'
    },
    secondUser: {
      id: 'temp2',
      image: require('./../../assets/images/user11.png'),
      name: 'Matti Renatta',
      occupation: 'Software Engineer',
      experience: '3yrs exp'
    }
  },
  {
    firstUser: {
      id: 'temp3',
      image: require('./../../assets/images/user13.png'),
      name: 'Amanda Frost',
      occupation: 'School Teacher',
      experience: '7yrs exp'
    },
    secondUser: {
      id: 'temp4',
      image: require('./../../assets/images/user14.png'),
      name: 'Matti Renatta',
      occupation: 'Software Engineer',
      experience: '3yrs exp'    
    }
  }
]

export default PendingRequest = () => {
  return (
    <FlatList
      data={pendingRequestList}
      keyExtractor={item => item.firstUser.id}
      renderItem={({item}) => <PendingRequestItem {...item} />}
    />
  )
}

const PendingRequestItem = ({firstUser, secondUser}) => {
  return (
    <View style={styles.requestWrapper}>
      <View style={styles.imageWrapper}>
          <Image source={firstUser.image} style={styles.userImage} />
        <View style={styles.divider} />
        <View style={styles.handshakeWrapper}>
          <Image source={require('./../../assets/images/handshake.png')} styles={styles.handshakeImage} />
        </View>
        <View style={styles.divider} />
          <Image source={secondUser.image} style={styles.userImage} />
      </View>
      <View style={styles.contentWrapper}>
        <View style={styles.detailWrapper}>
          <Text style={styles.name}>{firstUser.name}</Text>
          <Text style={styles.occupation}>{firstUser.occupation}</Text>
          <Text style={styles.experience}>{firstUser.experience}</Text>
        </View>
        <View style={styles.actionWrapper}>
          <View style={[styles.action, styles.actionMsg]}>
            <Image source={require('./../../assets/images/msg.png')} />
          </View>
          <View style={[styles.action, styles.actionCancel]}>
            <Image source={require('./../../assets/images/close.png')} />
          </View>
        </View>
        <View style={styles.detailWrapper}>
        <Text style={styles.name}>{secondUser.name}</Text>
          <Text style={styles.occupation}>{secondUser.occupation}</Text>
          <Text style={styles.experience}>{secondUser.experience}</Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  requestWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: colors.borderColor,
    paddingVertical: 25,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: setWidth(5),
  },
  imageWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  divider: {
    width: setWidth(8),
    backgroundColor: '#D9D9D9',
    height: 2
  },
  userImage: {
    width: setWidth(20),
    height: setWidth(20),
    marginHorizontal: setWidth(2.5)
  },
  handshakeWrapper: {
    marginHorizontal: setWidth(3)
  },
  handshakeImage: { 
    width: setWidth(5),
    height: setWidth(5)
  },
  contentWrapper: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    marginTop: 5
  },
  detailWrapper: {
    width: setWidth(30)
  },
  name: {
    textAlign: 'center',
    fontFamily: fonts.primary,
    fontSize: 15,
    color: '#000000'
  },
  occupation: {
    textAlign: 'center',
    fontFamily: fonts.primary,
    fontSize: 14,
    color: '#9B9B9B'
  },
  experience: {
    textAlign: 'center',
    fontFamily: fonts.primary,
    fontSize: 10,
    color: '#C3C3C3'
  },
  actionWrapper: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  actionMsg: {
    marginRight: 3
  },
  actionCancel: {
    marginLeft: 3
  }
})