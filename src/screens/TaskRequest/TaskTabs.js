import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image } from 'react-native';
import { setWidth, setHeight } from '../../utils';
import { colors, fonts } from '../../constant/style';
import PendingRequest from './PendingRequest';
import RequestList from './RequestList';

const dummy = 'You have already 3 tasks to complete. Hurry and get on them before it’s too late to reverse your score.';


export default TaskTabs = () => {
  const [activeTab, setActiveTab] = useState('PENDING TASKS')

  return (
    <>
    <View style={styles.wrapper}>
      <Tab title={"PENDING TASKS"} isActive={activeTab == 'PENDING TASKS'} onPress={setActiveTab} notification />
      <Tab title={"COMPLETED TASKS"} isActive={activeTab == 'COMPLETED TASKS'} onPress={setActiveTab} />
    </View>
    {activeTab == "PENDING TASKS" ? (
      <>
        <View style={{justifyContent: 'center', alignItems: 'center', paddingVertical: 30, paddingHorizontal: 40, backgroundColor: colors.subBgColor}}>
      <Image source={require('./../../assets/images/warning.png')} />
      <Text style={styles.warningText}>{dummy}</Text>
    </View>
    <PendingRequest />
    <RequestList />
      </>
    ) : <View />}
    
    </>
  )
}

const Tab = ({title, isActive, onPress, notification}) => {
  return (
    <TouchableOpacity style={[styles.tabWrapper, {borderBottomColor: isActive ? '#F2994A' : 'transparent'}]} activeOpacity={1} onPress={() => {
      onPress(title)
    }}
      
    >
      <Text style={[styles.tabName, { color: isActive ? '#727270' : '#72727030'}]}>{title}</Text>
      <View style={[styles.notification, {backgroundColor: notification && isActive ? '#D0021B' : notification ? '#D0021B30': 'transparent'}]} />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    width: setWidth('100'),
    backgroundColor: '#FFFFFF'
  },
  tabWrapper: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
    paddingVertical: setHeight('1.5'),
    borderBottomWidth: 1,
    marginHorizontal: setWidth(7),
    flexDirection: 'row'
  },
  tabName: {
    fontFamily: fonts.primaryBold,
    fontSize: 12,
  },
  notification: {
    width: setWidth(2),
    height: setWidth(2),
    borderRadius: setWidth(2),
    marginLeft: 5
  }
})