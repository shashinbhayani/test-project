import React from 'react';
import { View, Text, ScrollView, Image, StyleSheet } from 'react-native'
import Header from './Header';
import MainTabs from './MainTabs';
import TaskTabs from './TaskTabs';
import { colors, fonts } from '../../constant/style';
import { setHeight } from '../../utils';

export default TaskRequest = (props) => {

  return (
    <>
      <ScrollView persistentScrollbar={true} contentContainerStyle={{marginBottom: 20, backgroundColor: colors.bgColor}} showsVerticalScrollIndicator={false}>
        <Header navigation={props.navigation} />
        <MainTabs />
        <TaskTabs ref={res => this.taskTabRef = res} />
        
      </ScrollView>
    </>
  )
}

const styles = StyleSheet.create({
  warningText: {
    textAlign: "center",
    fontSize: 18,
    color: '#323E4670',
    lineHeight: setHeight(4.3),
    paddingTop: 10,
    fontFamily: fonts.primary
  }
})