import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import { setWidth, setHeight } from '../../utils';
import { fonts, colors } from '../../constant/style';
import Swipeable from 'react-native-swipeable';

const requestList = [
  {
    firstUser: {
      id: 'temp1',
      image: require('./../../assets/images/user15.png'),
      name: 'Amanda Frost',
      occupation: 'School Teacher',
      experience: '7yrs exp'
    },
    secondUser: {
      id: 'temp2',
      image: require('./../../assets/images/user16.png'),
      name: 'Matti Renatta',
      occupation: 'Software Engineer',
      experience: '11yrs exp'
    },
    is_delete: false
  },
  {
    firstUser: {
      id: 'temp3',
      image: require('./../../assets/images/user14.png'),
      name: 'Amanda Frost',
      occupation: 'School Teacher',
      experience: '7yrs exp'
    },
    secondUser: {
      id: 'temp4',
      image: require('./../../assets/images/user15.png'),
      name: 'Matti Renatta',
      occupation: 'Software Engineer',
      experience: '11yrs exp'
    },
    is_delete: false
  },
  {
    firstUser: {
      id: 'temp5',
      image: require('./../../assets/images/user17.png'),
      name: 'Dameon Peterson',
      occupation: 'School Teacher',
      experience: '7yrs exp'
    },
    secondUser: {
      id: 'temp6',
      image: require('./../../assets/images/user18.png'),
      name: 'Abhoy Latif',
      occupation: 'Software Engineer',
      experience: '11yrs exp'
    },
    is_delete: true
  }
]

export default class RequestList extends React.Component {

  renderBottomRow = ({ item, index }) => {
    const { firstUser, secondUser, is_delete } = item
    const itemProps = {
      onOpen: (event, gestureState, swipeable) => {
        if (this.currentlyOpenSwipeable && this.currentlyOpenSwipeable !== swipeable) {
          this.currentlyOpenSwipeable.recenter();
        }
        this.currentlyOpenSwipeable = swipeable;
      },
      onClose: () => { this.currentlyOpenSwipeable = null }
    };
    const rightButtons = is_delete ? null : [
      <TouchableOpacity style={{ backgroundColor: colors.green, flex: 1, justifyContent: 'center', padding: setWidth(7) }}>
        <Image source={require('./../../assets/images/pin.png')} style={{ height: setHeight(3), width: setWidth(6) }} resizeMode='contain'></Image>
      </TouchableOpacity>,
      <TouchableOpacity style={{ backgroundColor: colors.red, flex: 1, justifyContent: 'center', padding: setWidth(8) }}>
        <Image source={require('./../../assets/images/close-white.png')} style={{ height: setHeight(2), width: setWidth(4) }} resizeMode='contain'></Image>
      </TouchableOpacity>];
    return (
      <Swipeable
        onSwipeStart={() => this.setState({ enableScroll: false })}
        onSwipeRelease={() => this.setState({ enableScroll: true })}
        rightButtons={rightButtons}
        onRightButtonsOpenRelease={itemProps.onOpen}
        onRightButtonsCloseRelease={itemProps.onClose}
        rightButtonWidth={setWidth(20)}
        key={index} >
        <View style={[styles.itemWrapper, { opacity: is_delete ? 0.7 : 1 }]}>
          <View style={[styles.detailWrapper, { justifyContent: 'flex-end' }]}>
            <View style={{ marginRight: 10 }}>
              <Text style={[styles.name, { textAlign: 'right' }]} ellipsizeMode="tail" numberOfLines={1}>{firstUser.name}</Text>
              <Text style={[styles.occupation, { textAlign: 'right' }]} ellipsizeMode="tail" numberOfLines={1}>{firstUser.occupation}</Text>
              <Text style={[styles.experience, { textAlign: 'right' }]} ellipsizeMode="tail" numberOfLines={1}>{firstUser.experience}</Text>
            </View>
            <Image source={firstUser.image} style={styles.image} />
          </View>
          <View style={{ width: setWidth(5), height: 2, backgroundColor: '#D9D9D9' }} />
          <View style={[styles.detailWrapper, { justifyContent: 'flex-start' }]}>
            <Image source={secondUser.image} style={styles.image} />
            <View style={{ marginLeft: 10 }}>
              <Text style={[styles.name, { textAlign: 'left' }]} ellipsizeMode="tail" numberOfLines={1}>{secondUser.name}</Text>
              <Text style={[styles.occupation, { textAlign: 'left' }]} ellipsizeMode="tail"numberOfLines={1}>{secondUser.occupation}</Text>
              <Text style={[styles.experience, { textAlign: 'left' }]} ellipsizeMode="tail"numberOfLines={1}>{secondUser.experience}</Text>
            </View>
          </View>
        </View>
        {is_delete &&
          <View style={styles.itemBlurView}>
            <Image source={require('./../../assets/images/cancel.png')} style={{ height: setHeight(1.5), width: setWidth(3) }} resizeMode='contain'></Image>
          </View>
        }
      </Swipeable>
    )
  }

  render() {

    return (
      <FlatList
        ref={ref => this._list = ref}
        scrollEnabled={false}
        showsVerticalScrollIndicator={false}
        data={requestList}
        renderItem={this.renderBottomRow}
        keyExtractor={(item, index) => {
          return index + "";
        }}
        ItemSeparatorComponent={() => (
          <View style={{ height: 1.5, backgroundColor: '#E8E8E8' }} />
        )}
      />
    )
  }
}


const styles = StyleSheet.create({
  itemWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.bgColor,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#E8E8E8',
    alignSelf: 'center',
    overflow: 'hidden'
  },
  detailWrapper: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  name: {
    textAlign: 'right',
    fontFamily: fonts.primary,
    fontSize: 15,
    maxWidth: setWidth(30)
  },
  occupation: {
    textAlign: 'right',
    fontFamily: fonts.primary,
    fontSize: 14,
    color: '#9B9B9B',
    maxWidth: setWidth(30)
  },
  experience: {
    textAlign: 'right',
    fontFamily: fonts.primary,
    fontSize: 10,
    color: '#C3C3C3',
    maxWidth: setWidth(30)
  },
  image: {
    width: setWidth(10),
    height: setWidth(10)
  },
  itemBlurView: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fffffdd0',
    height: setHeight(10),
    width: '100%'
  }
})