import React from 'react';
import { View, Text, Image, StyleSheet, FlatList } from 'react-native'
import { colors, fonts } from '../../constant/style';
import { setWidth, setHeight } from '../../utils';
import * as Progress from 'react-native-progress';

const dummyText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullam Lorem ipsum dolor sit amet, consecte!';

const PotatoesList = [{
  title: 'Lorem ipsum dolo',
  inquiries: 14,
  percent: 63
},{
  title: 'Meternity',
  inquiries: 5,
  percent: 34
},{
  title: 'Randomly',
  inquiries: 2,
  percent: 21
}]

export default YumPotatoes = (props) => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.mainText}>{dummyText}</Text>     
      <View style={styles.potatoesWrapper}>
        <View style={styles.potatoesHeaderWrapper}>
          <Text style={styles.potatoesHeader}>Your Yam Potatoes</Text>
          <Text style={styles.potatoesSubHeader}>Sun 5 - Sat 11</Text>
        </View>
      </View>
      <FlatList
        data={PotatoesList}
        contentContainerStyle={{marginLeft: setWidth('5'), marginTop: 10}}
        keyExtractor={(item) => item.title}
        renderItem={({item}) => <PotatoesListItem {...item} /> }
      />
    </View>
  )
}

const PotatoesListItem = ({title, inquiries, percent}) => {
  return (
    <View style={styles.itemWrapper}>
      <View style={{width: setWidth('40')}}>
        <Text style={styles.itemTitle}>{title}</Text>
      </View>
      <Text style={{
        fontFamily: fonts.primary,
        fontSize: 14,
        color: '#575757',
        width: setWidth('30')
      }}>
        {inquiries}
        <Text style={{color: '#9B9B9B'}}> Inquiries</Text>
      </Text>
      <View style={{width: setWidth('20')}}>
        <Progress.Bar progress={(percent/100)} width={setWidth('20')} color={'#838383'} unfilledColor={'#BABABA30'} borderWidth={0} height={3} />
        <Text style={styles.activeText}>
          {`${percent}%`}
          <Text style={{color: '#9B9B9B'}}> Active</Text>
          </Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.bgColor,
    borderBottomWidth: 1,
    borderBottomColor: colors.borderColor,
  },
  mainText: {
    fontFamily: fonts.primary,
    paddingHorizontal: setWidth(5),
    fontSize: 18,
    lineHeight: 25,
    paddingVertical: 15,
    borderBottomColor: colors.borderColor,
    borderBottomWidth: 1,
    color: '#4A4A4A'
  },
  potatoesWrapper: {
    marginHorizontal: setWidth(5),
  },
  potatoesHeaderWrapper: {
    flexDirection: 'row',
    marginVertical: 10,
    alignContent: "flex-end"
  },
  potatoesHeader: {
    fontFamily: fonts.primaryMedium,
    fontSize: 20,
    marginRight: setWidth(1),
    textAlignVertical: 'bottom'
  },
  potatoesSubHeader: {
    fontFamily: fonts.primaryMedium,
    fontSize: 17,
    textAlignVertical: 'bottom',
    color: colors.headerPostfixColor,
    paddingLeft: 10
  },
  itemWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10
  },
  itemTitle: {
    fontFamily: fonts.primary,
    fontSize: 14,
    backgroundColor: colors.subBgColor,
    color: '#727270',
    paddingHorizontal: 7,
    paddingVertical: 3,
    borderRadius: 4,
    alignSelf: 'flex-start'
  },
  activeText: {
    fontSize: 16,
    color: '#575757'
  }
})