import React from 'react';
import { View, Text, ScrollView } from 'react-native'
import RandomizedPersonTopBar from './RandomizedPersonTopBar';
import YumPotatoes from './YumPotatoes';
import RiceGrains from './RiceGrains';
import Header from './Header';
import { colors } from '../../constant/style';

export default RandomizedPerson = (props) => {
  return (
    <>
    <Header navigation={props.navigation} />
      <ScrollView persistentScrollbar={true} contentContainerStyle={{paddingBottom: 20, backgroundColor: colors.bgColor}} showsHorizontalScrollIndicator={true} >
      <RandomizedPersonTopBar />
      <YumPotatoes />
      <RiceGrains />
    </ScrollView>
    </>
  )
}