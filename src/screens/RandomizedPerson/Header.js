import React from 'react';
import { View, Text, Platform, StyleSheet, Image, TouchableHighlight } from 'react-native';
import { setHeight, setWidth } from '../../utils';
import { fonts } from '../../constant/style';
 
const headerHeight = Platform.OS == 'ios' ? 64 : 56

export default Header = ({navigation}) => {
  return (
    <View style={styles.headerWrapper}>
      <TouchableHighlight onPress={() => navigation.pop()}>
        <Image source={require('../../assets/images/Arrow.png')} />
      </TouchableHighlight>
      <Text style={styles.title}>{'Randomized Person'}</Text>
      <Text style={{color: '#E06732'}}>Edit</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  headerWrapper: {
    height: headerHeight,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: setWidth(2),
    alignItems: 'center',
    elevation: 2
  },
  title: {
    fontFamily: fonts.primaryMedium,
    fontSize: 21,
    color: '#1C252A'
  }
})