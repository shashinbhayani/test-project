import React from 'react';
import { View, Text, Image, ImageBackground, StyleSheet } from 'react-native'
import { setWidth, setHeight } from '../../utils';
import { fonts, colors } from '../../constant/style';


export default RandomizedPersonTopBar = (props) => {
  return (
    <View>
      <ImageBackground
        source={require('./../../assets/images/Rectangle.png')}
        style={styles.imageWrapper}
      >
        <View>
          <Image
            source={require('./../../assets/images/user19.png')}
          />
          <Image
            source={require('./../../assets/images/Star.png')}
            style={styles.starImage}
          />
        </View>
        <View>
          <View style={{flexDirection: "row", justifyContent: 'space-between', width: setWidth(40), marginBottom: 10}}>
            <BigActivity title={'LIFE RATE'} value={69} valuePostfix={'%'} />
            <BigActivity title={'EXPERIENCE'} value={21} valuePostfix={'Yrs'} />
          </View>
          <View style={{flexDirection: "row"}}>
            <BigActivity title={'LAZINESS RATE'} value={93} valuePostfix={'%'} />
          </View>
        </View>
      </ImageBackground>
      <View style={styles.bottomWrapper}>
        <Text style={styles.title}>Lorem ipsum dolor sit amet</Text>
        <View style={styles.subTitleWrapper}>
          <Image 
            source={require('./../../assets/images/cats-need-icon-13.png')}
            style={styles.subTitleImage}
            />
          <Text style={styles.subTitle}>Lorem ipsum dolor</Text>
        </View>
      </View>
    </View>
  )
}

const BigActivity = ({title, value, valuePostfix}) => {
  return (
    <View style={{ marginHorizontal: setWidth('2')}}>
      <Text style={{fontFamily: fonts.primary, color: '#33353D', fontSize: 20}}>
        {value}
        <Text style={{fontFamily: fonts.primary, fontSize: 10}}>{valuePostfix}</Text>
      </Text>
      <Text style={{fontFamily: fonts.primary, color: '#727270', fontSize: 10}}>{title}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  imageWrapper: {
    width: setWidth('100%'), 
    height: setHeight(19), 
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: setWidth(5),
    elevation: 1
  },
  profileImage: {
    width: setWidth(30),
    height: setWidth(30)
  },
  starImage: {
    width: setWidth(8),
    height: setWidth(8),
    position: 'absolute',
    bottom: setWidth(5),
    right: setWidth(5)
  },
  bottomWrapper: {
    paddingLeft: setWidth(5),
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: colors.borderColor,
    backgroundColor: colors.bgColor
  },
  title: {
    fontFamily: fonts.primaryMedium,
    fontSize: 20
  },
  subTitleWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
  },
  subTitleImage: {
    marginRight: 15
  },
  subTitle: {
    fontFamily: fonts.primaryMedium,
    fontSize: 15
  }
})