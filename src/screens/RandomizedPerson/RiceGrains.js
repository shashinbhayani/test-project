import React from 'react';
import { View, Text, Image, StyleSheet, FlatList } from 'react-native'
import { colors, fonts } from '../../constant/style';
import { setWidth, setHeight } from '../../utils';
import * as Progress from 'react-native-progress';

const riceGrainList = [
  {
    title: "Dummy 1",
    image: require('./../../assets/images/1.png'),
    views: 23,
    click: 8,
    percent: 63
  },
  {
    title: "Dummy 2",
    image: require('./../../assets/images/4.png'),
    views: 15,
    click: 3,
    percent: 49
  },
  {
    title: "Dummy 3",
    image: require('./../../assets/images/2.png'),
    views: 7,
    click: 1,
    percent: 34
  },
  {
    title: "Dummy 4",
    image: require('./../../assets/images/5.png'),
    views: 5,
    click: 0,
    percent: 12
  },
  {
    title: "Dummy 5",
    image: require('./../../assets/images/3.png'),
    views: 2,
    click: 0,
    percent: 8
  }
]


export default RiceGrains = (props) => {
  return (
    <>
      <View style={styles.riceWrapper}>
        <View style={styles.riceHeaderWrapper}>
          <Text style={styles.riceHeader}>Your Rice Graind</Text>
          <Text style={styles.riceSubHeader}>Sun 5 - Sat 11</Text>
        </View>
      </View>
      <FlatList
        data={riceGrainList}
        contentContainerStyle={{paddingHorizontal: setWidth('5'), backgroundColor: colors.bgColor}}
        keyExtractor={(item) => item.title}
        renderItem={({item}) => <RiceGrainListItem {...item} /> }
      />
    </>
  )
}

const RiceGrainListItem = ({image, views, click, percent}) => {
  return (
    <View style={styles.itemWrapper}>
      <Image
        source={image}
        style={{width: setWidth(18), height: setWidth(18)}}
      />
      <View>
        <Text style={styles.views}>
          {views}
          <Text style={{color: '#9B9B9B'}}> Views</Text>
        </Text>
        <Text style={styles.views}>
          {click}
          <Text style={{color: '#9B9B9B'}}> Click-troughs</Text>
        </Text>
      </View>
      <View style={{width: setWidth('20')}}>
        <Progress.Bar progress={(percent/100)} width={setWidth('20')} color={'#838383'} unfilledColor={'#BABABA30'} borderWidth={0} height={3} />
        <Text style={styles.activeText}>
          {`${percent}%`}
          <Text style={{color: '#9B9B9B'}}> Active</Text>
          </Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.bgColor,
  },
  riceWrapper: {
    paddingHorizontal: setWidth(5),
    backgroundColor: colors.bgColor,
    paddingVertical: 10
  },
  riceHeaderWrapper: {
    flexDirection: 'row',
    marginVertical: 10,
    alignContent: "flex-end"
  },
  riceHeader: {
    fontFamily: fonts.primaryMedium,
    fontSize: 20,
    marginRight: 2,
    textAlignVertical: 'bottom'
  },
  riceSubHeader: {
    fontFamily: fonts.primaryMedium,
    fontSize: 17,
    textAlignVertical: 'bottom',
    color: colors.headerPostfixColor,
    paddingLeft: 10
  },
  itemWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  activeText: {
    fontSize: 16,
    color: '#575757'
  },
  views: {
    fontFamily: fonts.primary,
    fontSize: 15,
    color: '#575757',
    width: setWidth('30')
  }
})