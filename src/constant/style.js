
export const fonts = {
  primary: "SFProText Regular",
  primaryMedium: "SFProText Medium",
  primarySemibold: "SFProText Semibold",
  primaryBold: "SF Pro Text Bold",
}


export const colors = {
  borderColor: '#EBEBEB',
  bgColor: '#F8F8F8',
  subBgColor: '#F2F2F2',
  headerColor: '#000000',
  headerPostfixColor: '#B1B1B1',
  yellow: '#F2994A',
  lightGray: '#E8E8E8',
  white: '#ffffff',
  black: '#000000',
  red: '#D0021B',
  gray: '#D9D9D9',
  green: '#43B681',
  grayBackground: '#F8F8F8',
  
  sepratorColor: '#E8E8E8',

  blackFont: '#080808',
  grayFont: '#727270',
  warningFont: '#323E46',
  lightGrayText: '#C3C3C3'
}