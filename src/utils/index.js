import {widthPercentageToDP, heightPercentageToDP} from 'react-native-responsive-screen';
 
export const setHeight = (height) => heightPercentageToDP(height)
export const setWidth = (height) => widthPercentageToDP(height)
